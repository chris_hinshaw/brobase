# Makefile for brobase
ROOTDIR=$(PWD)
PLUGIN_INSTALL=--bro-dist=$(ROOTDIR)/bro --install-root=$(ROOTDIR)/install/usr/local/bro/lib/bro/plugins
MAKEFLAGS=--no-print-directory --slient 
NUM_PROC=$(shell echo `grep -c processor /proc/cpuinfo`)
BRO_BRANCH=master

all: make_jemalloc make_bro af_packet 
	@echo $@ completed

git_src: jemalloc/README bro/README bro-af_packet-plugin/README 

do_configs: git_src
	cd jemalloc && CFLAGS=-fPIC ./configure --prefix=$(ROOTDIR)/install --disable-thp && make && make install
	cd bro && LDFLAGS=-L$(ROOTDIR)/install/lib LIBS="-ljemalloc `$(ROOTDIR)/install/bin/jemalloc-config --libs`" \
	./configure --prefix=$(ROOTDIR)/install/usr/local/bro --enable-jemalloc
	cd bro-af_packet-plugin/ && ./configure $(PLUGIN_INSTALL) --with-kernel=4.10.13-1.el7.elrepo.x86_64

distclean:
	make -C jemalloc $@
	make -C bro $@
	make -C bro-af_packet-plugin $@
	rm -rf install

destroy:
	-rm -rf jemalloc bro bro-af_packet-plugin install

jemalloc/README:
	git clone https://github.com/jemalloc/jemalloc.git -b 5.1.0

jemalloc/Makefile: jemalloc/README 
	cd jemalloc && CFLAGS=-fPIC ./autogen.sh --prefix=$(ROOTDIR)/install --with-malloc-conf="thp:never" --disable-initial-exec-tls

make_jemalloc: jemalloc/Makefile
	make -j$(NUM_PROC) -C jemalloc all
	make -C jemalloc install_bin install_include install_lib

bro/README: export BRO_BRANCH=v2.5.5
bro/README:
	git clone --recursive git://git.bro.org/bro bro -b $(BRO_BRANCH)

make_bro: bro/README
	cd bro && LDFLAGS=-L$(ROOTDIR)/install/lib LIBS="-ljemalloc `$(ROOTDIR)/install/bin/jemalloc-config --libs`" \
	./configure --prefix=$(ROOTDIR)/install/usr/local/bro --enable-debug --enable-jemalloc --with-jemalloc=$(ROOTDIR)/install
	make -j$(NUM_PROC) -C bro
	make -C bro install INSTALL="install -p"

bro-af_packet-plugin/README: 
	git clone https://github.com/J-Gras/bro-af_packet-plugin.git bro-af_packet-plugin

af_packet: bro-af_packet-plugin/README bro/README
	cd bro-af_packet-plugin/ && ./configure $(PLUGIN_INSTALL) --with-kernel=4.10.13-1.el7.elrepo.x86_64
	make -C bro-af_packet-plugin
	make -C bro-af_packet-plugin install

run_pcap: export BRO_PATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_pcap: export BROPATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_pcap: export BRO_PLUGIN_PATH=$(PWD)/install/usr/local/bro/lib/bro/plugins
run_pcap: export BRO_DNS_FAKE=on
run_pcap: export PROBE_KAFKA_BROKERS=
run_pcap:
	if [ -z $(PCAP_FILE) ] ; then exit 1; fi
	cp -f $(PWD)/bro/aux/broctl/etc/node.cfg $(PWD)/install/usr/local/bro/etc/.
	$(PWD)/bro/build/src/bro -r $(PCAP_FILE) 2>&1 | tee logbro.log

# NOTE:  It will help to disable SSL.
setup_rootfs:
	cp -f $(PWD)/node.cfg $(PWD)/install/usr/local/bro/etc/.
	@echo "need to add scripts folder"

run_ip_test: export BRO_PATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_ip_test: export BROPATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_ip_test: export BRO_PLUGIN_PATH=$(PWD)/install/usr/local/bro/lib/bro/plugins
run_ip_test: export BRO_DNS_FAKE=on
run_ip_test: export PROBE_KAFKA_BROKERS=
run_ip_test: setup_rootfs
	gdb $(PWD)/bro/build/src/bro -CQ -i ens36 2>&1 | tee logbro.log

run_test: export BRO_PATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_test: export BROPATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_test: export BRO_PLUGIN_PATH=$(PWD)/install/usr/local/bro/lib/bro/plugins
run_test: export BRO_DNS_FAKE=on
run_test: export PROBE_KAFKA_BROKERS=
run_test: setup_rootfs
	$(PWD)/bro/build/src/bro -CQ -i ens36 2>&1 | tee logbro.log

run_vg_test: export BRO_PATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_vg_test: export BROPATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/usr/local/bro/share/bro/site
run_vg_test: export BRO_PLUGIN_PATH=$(PWD)/install/usr/local/bro/lib/bro/plugins
run_vg_test: export BRO_DNS_FAKE=on
run_vg_test: export PROBE_KAFKA_BROKERS=
run_vg_test: setup_rootfs
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --demangle=yes --log-file=$(PWD)/valgrindbro.log $(PWD)/bro/build/src/bro -Q -r $(PWD)/bro/testing/btest/Traces/smb/smb2.pcap
	valgrind --tool=callgrind --demangle=yes --log-file=$(PWD)/callgrindbro.log $(PWD)/bro/build/src/bro -Q -r $(PWD)/bro/testing/btest/Traces/smb/smb2.pcap

run_je_test: export BROPATH=.:$(PWD)/install/usr/local/bro/share/bro:$(PWD)/install/usr/local/bro/share/bro/policy:$(PWD)/install/usr/local/bro/share/bro/site
run_je_test: export BRO_PLUGIN_PATH=$(PWD)/install/usr/local/bro/lib/bro/plugins
run_je_test: export BRO_DNS_FAKE=on
run_je_test: export PROBE_KAFKA_BROKERS=
run_je_test: setup_rootfs
	@rm -rf /tmp/jeprof.$@* *.schema .state
	MALLOC_CONF=prof_leak:true,lg_prof_sample:0,prof_final:true,prof_prefix:/tmp/jeprof.$@ LD_PRELOAD=$(ROOTDIR)/install/lib/libjemalloc.so.2 \
	$(PWD)/install/usr/local/bro/bin/bro -CQ -i ens36 &
	sleep 120
	killall -q bro
	sleep 10
	$(PWD)/jemalloc/bin/jeprof --inuse_space --lines --heapcheck --svg $(PWD)/install/usr/local/bro/bin/bro /tmp/jeprof.$@* > $@.`date '+%Y%m%d_%H:%M:%S'`.svg


